import sys, os
from operator import itemgetter

if len(sys.argv) < 2:
    sys.exit("Usage: %s filename" % sys.argv[0])
filename = sys.argv[1]
if not os.path.exists(filename):
    sys.exit("Error: File '%s' not found" % sys.argv[1])

f = open(filename)
cardinals_1940 = f.read()
#print cardinals_1940;

import re
 
stats_regex = re.compile(r"\b(?P<name>\w+\s\w+)\sbatted\s(?P<num_bats>\d)\stimes\swith\s(?P<num_hits>\d)\shits\sand\s(?P<num_runs>\d)\sruns")

def find_all_stats(test):
    return stats_regex.findall(test)

all_stats = find_all_stats(cardinals_1940)

#names_list = [x[0] for x in all_stats]
#num_bats_list = [x[1] for x in all_stats]
#num_hits_list = [x[2] for x in all_stats]
#num_runs_list = [x[3] for x in all_stats]

players = []
players.append("Sam Shapiro") #list can't be empty when iterating through in the following for loop
for t in all_stats:
    name_already_in_list = False
    for n in players:
        if t[0] == n:
            name_already_in_list = True
            break
    if name_already_in_list == False:
        players.append(t[0])
players.pop(0) #delete "Sam Shapiro" element added above

players_avgs = [0]*len(players)
x = 0
for n in players:
    num_bats = 0.0
    num_hits = 0.0
    for t in all_stats:
        if t[0] == n:
            num_bats += int(t[1])
            num_hits += int(t[2])
    batting_average = "%.3f" % round(num_hits/num_bats,3)
    players_avgs[x] = batting_average
    x+=1
    
final = zip(players,players_avgs)
final = sorted(final,key=itemgetter(1), reverse=True)

for n in final:
    print n[0] + ": " + n[1]


